package ru.karamyshev.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.model.User;

import java.util.List;

public interface IUserRepository {

    @NotNull
    List<User> findAll();

    @NotNull
    User add(@NotNull User user);

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @NotNull
    User removeUser(@NotNull User user);

    @NotNull
    User removeById(@NotNull String id);

    @Nullable
    User removeByLogin(@NotNull String login);

    @Nullable
    List<User> getUsersList();

    @Nullable
    void add(@NotNull List<User> userList);

    @Nullable
    void load(@NotNull List<User> userList);
}
