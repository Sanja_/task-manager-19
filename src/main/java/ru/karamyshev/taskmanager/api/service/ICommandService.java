package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

public interface ICommandService {

    @Nullable
    List<AbstractCommand> getTerminalCommands();

}
