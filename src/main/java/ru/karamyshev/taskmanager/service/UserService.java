package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.IUserRepository;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.exception.NotLockYourUserException;
import ru.karamyshev.taskmanager.exception.NotRemoveYourUserException;
import ru.karamyshev.taskmanager.exception.empty.*;
import ru.karamyshev.taskmanager.model.User;
import ru.karamyshev.taskmanager.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public User findById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.findById(id);
    }

    @Nullable
    @Override
    public User findByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    public User removeUser(final @Nullable User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Nullable
    @Override
    public User removeById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.removeById(id);
    }

    @Nullable
    @Override
    public User removeByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return null;
    }

    @Nullable
    @Override
    public User create(final @Nullable String login, final @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Nullable
    @Override
    public User create(
            final @Nullable String login,
            final @Nullable String password,
            final @Nullable String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Nullable
    @Override
    public User create(
            final @Nullable String login,
            final @Nullable String password,
            final @Nullable Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return null;
    }

    @Nullable
    @Override
    public User lockUserByLogin(final @Nullable String currentLogin, final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) return null;
        if (currentLogin.equals(login)) throw new NotLockYourUserException();
        user.setLocked(true);
        return user;
    }

    @Nullable
    @Override
    public User unlockUserByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return user;
    }

    @Nullable
    @Override
    public User removeUserByLogin(final @Nullable String currentLogin, final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = findByLogin(login);
        if (user == null) return null;
        if (currentLogin.equals(login)) throw new NotRemoveYourUserException();
        removeUser(user);
        return user;
    }

    @Nullable
    @Override
    public void load(@Nullable List<User> usersList) {
        if (usersList == null) return;
        userRepository.load(usersList);
    }

    @Nullable
    @Override
    public List<User> getUsersList() {
        return userRepository.getUsersList();
    }

}
