package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.service.IDomainService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.dto.Domain;

public class DomainService implements IDomainService {

    private final IUserService userService;

    private final ITaskService taskService;

    private final IProjectService projectService;

    public DomainService(IUserService userService, ITaskService taskService, IProjectService projectService) {
        this.userService = userService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Nullable
    @Override
    public void load(final @Nullable Domain domain) {
        if (domain == null) return;
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
        userService.load(domain.getUsers());
    }

    @Nullable
    @Override
    public void export(final @Nullable Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.getProjectsList());
        domain.setTasks(taskService.getTasksList());
        domain.setUsers(userService.getUsersList());
    }

}
