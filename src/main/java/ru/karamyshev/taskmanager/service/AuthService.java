package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.enumerated.Role;
import ru.karamyshev.taskmanager.exception.MatchLoginsException;
import ru.karamyshev.taskmanager.exception.NotMatchPasswordsException;
import ru.karamyshev.taskmanager.exception.empty.EmptyLoginException;
import ru.karamyshev.taskmanager.exception.empty.EmptyPasswordException;
import ru.karamyshev.taskmanager.exception.empty.EmptyUserIdException;
import ru.karamyshev.taskmanager.exception.user.AccessDeniedException;
import ru.karamyshev.taskmanager.model.User;
import ru.karamyshev.taskmanager.util.HashUtil;

public class AuthService implements IAuthService {

    private IUserService userService;

    private String userId;

    private String currentLogin;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Nullable
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Nullable
    @Override
    public void checkRoles(final Role @Nullable [] roles) {
        if (roles == null || roles.length == 0) return;
        final String userId = getUserId();
        final User user = userService.findById(userId);
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) if (role.equals(item)) return;
        throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public String getCurrentLogin() {
        if (currentLogin == null) throw new AccessDeniedException();
        return currentLogin;
    }

    @Nullable
    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Nullable
    @Override
    public void login(final @Nullable String login, final @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        String userUpdateId = Long.toString(user.getId());
        currentLogin = user.getLogin();
        userId = userUpdateId;
    }

    @NotNull
    @Override
    public void logout() {
        userId = null;
    }

    @Nullable
    @Override
    public void registry(
            final @Nullable String login,
            final @Nullable String password,
            final @Nullable String email
    ) {
        userService.create(login, password, email);
    }

    @Nullable
    @Override
    public void renamePassword(
            @Nullable String userId,
            @Nullable String currentLogin,
            @Nullable String oldPassword,
            @Nullable String newPassword
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new EmptyPasswordException();
        if (currentLogin == null || currentLogin.isEmpty()) throw new EmptyLoginException();
        final User user = userService.findByLogin(currentLogin);
        if (user == null) throw new AccessDeniedException();
        String hash = HashUtil.salt(oldPassword);
        if (!hash.equals(user.getPasswordHash())) throw new NotMatchPasswordsException();
        String newHash = HashUtil.salt(newPassword);
        user.setPasswordHash(newHash);
    }

    @Nullable
    @Override
    public void renameLogin(
            @Nullable String currentUserId,
            @Nullable String currentLogin,
            @Nullable String newLogin
    ) {
        if (currentUserId == null || currentUserId.isEmpty()) throw new EmptyUserIdException();
        if (currentLogin == null || currentLogin.isEmpty()) throw new EmptyLoginException();
        if (newLogin == null || newLogin.isEmpty()) throw new EmptyLoginException();
        final User user = userService.findByLogin(currentLogin);
        if (user == null) throw new AccessDeniedException();
        final User newLog = userService.findByLogin(newLogin);
        if (newLog != null) throw new MatchLoginsException();
        user.setLogin(newLogin);
        String userUpdateId = Long.toString(user.getId());
        userId = userUpdateId;
        this.currentLogin = user.getLogin();
    }

    @Nullable
    @Override
    public User showProfile(@Nullable String userId, @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

}
