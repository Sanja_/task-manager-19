package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.exception.IndexIncorrectException;
import ru.karamyshev.taskmanager.exception.empty.EmptyUserIdException;
import ru.karamyshev.taskmanager.exception.empty.IdEmptyException;
import ru.karamyshev.taskmanager.exception.empty.NameEmptyException;
import ru.karamyshev.taskmanager.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public void create(@Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Nullable
    @Override
    public void create(
            @Nullable String userId,
            final @Nullable String name,
            final @Nullable String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Nullable
    @Override
    public void add(@Nullable String userId, final @Nullable Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.add(userId, project);
    }

    @Nullable
    @Override
    public void remove(@Nullable String userId, final @Nullable Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.remove(userId, project);
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findAll(userId);
    }

    @Nullable
    @Override
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.clear(userId);
    }

    @Nullable
    @Override
    public Project findOneByIndex(@Nullable String userId, final @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index <= 0) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(userId, index - 1);
    }

    @Nullable
    @Override
    public List<Project> findOneByName(@Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return projectRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    public Project updateProjectById(
            @Nullable String userId,
            final @Nullable String id,
            final @Nullable String name,
            final @Nullable String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setId(Long.parseLong(id));
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneByIndex(@Nullable String userId, final @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index <= 0) throw new IndexIncorrectException();
        return projectRepository.removeOneByIndex(userId, index - 1);
    }

    @Nullable
    @Override
    public List<Project> removeOneByName(@Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return projectRepository.removeOneByName(userId, name);
    }

    @Override
    public Project findOneById(@Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public Project removeOneById(@Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.removeOneById(userId, id);
    }

    @Nullable
    @Override
    public Project updateProjectByIndex(
            @Nullable String userId,
            final @Nullable Integer index,
            final @Nullable String name,
            final @Nullable String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    @Override
    public void load(@Nullable List<Project> projects) {
        if (projects == null) return;
        projectRepository.load(projects);
    }

    @Nullable
    @Override
    public List<Project> getProjectsList() {
        return projectRepository.getProjectList();
    }

}
