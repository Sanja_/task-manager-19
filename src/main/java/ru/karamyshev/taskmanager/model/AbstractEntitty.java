package ru.karamyshev.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class AbstractEntitty {

    @NotNull
    private long id = UUID.randomUUID().getMostSignificantBits();

}
