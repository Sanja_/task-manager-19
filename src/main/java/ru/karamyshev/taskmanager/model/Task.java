package ru.karamyshev.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractEntitty implements Serializable {

    @NotNull
    private String name = "default name task";

    @NotNull
    private String description = "default description task";

    @NotNull
    private String userId;

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
