package ru.karamyshev.taskmanager.command.data.json.jax;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;

public class DataJaxJsonClearCommand extends AbstractDataCommand implements Serializable {

    @NotNull
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-jax-json-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove json(jax-b) file.";
    }

    @Nullable
    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE JSON(JAX-V) FILE]");
        final File file = new File(FILE_JAX_JSON);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
