package ru.karamyshev.taskmanager.command.info;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.service.ICommandService;
import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    public HelpCommand() {
    }

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Nullable
    @Override
    public void execute() {
        System.out.println("\n [HELP]");
        final ICommandService commandService = serviceLocator.getCommandService();
        final Collection<AbstractCommand> commands = commandService.getTerminalCommands();

        for (final AbstractCommand command : commands) {
            System.out.println(command.name() + " (" + command.arg() + ") : -" + command.description());

        }
        System.out.println("[OK]");
    }

}
