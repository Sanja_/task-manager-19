package ru.karamyshev.taskmanager.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.enumerated.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(IServiceLocator IServiceLocator) {
        this.serviceLocator = IServiceLocator;
    }

    @NotNull
    public Role[] roles() {
        return null;
    }

    @NotNull
    public abstract String arg();

    @NotNull
    public abstract String name();

    @NotNull
    public abstract String description();

    @Nullable
    public abstract void execute();

}
