package ru.karamyshev.taskmanager.command;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.dto.Domain;

import java.io.Serializable;

public class AbstractDataCommand extends AbstractCommand implements Serializable {

    protected static final String FILE_BINARY = "./data.bin";
    protected static final String FILE_BASE64 = "./data.base64";
    protected static final String FILE_FAST_JSON = "./data-fast.json";
    protected static final String FILE_FAST_XML = "./data-fast.xml";
    protected static final String FILE_JAX_JSON = "./data-jax.json";
    protected static final String FILE_JAX_XML = "./data-jax.xml";

    @Nullable
    public Domain getDomain() {
        final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().getProjectsList());
        domain.setTasks(serviceLocator.getTaskService().getTasksList());
        domain.setUsers(serviceLocator.getUserService().getUsersList());
        return domain;
    }

    @Nullable
    public void setDomain(final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().getProjectsList().clear();
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().getTasksList().clear();
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().getUsersList().clear();
        serviceLocator.getUserService().load(domain.getUsers());
    }

    @NotNull
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public void execute() {
    }

}
