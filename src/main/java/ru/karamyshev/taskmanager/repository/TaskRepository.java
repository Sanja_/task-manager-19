package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.ITaskRepository;
import ru.karamyshev.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @NotNull
    @Override
    public void add(final @NotNull String userId, final @NotNull Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @NotNull
    @Override
    public void remove(final @NotNull String userId, final @NotNull Task task) {
        tasks.remove(task);
    }

    @Nullable
    @Override
    public @NotNull List<Task> findAll(final @NotNull String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @NotNull
    @Override
    public void clear(final @NotNull String userId) {
        final List<Task> task = findAll(userId);
        this.tasks.removeAll(task);
    }

    @Nullable
    @Override
    public @NotNull Task findOneById(final @NotNull String userId, final @NotNull String id) {
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) {
                if (Long.parseLong(id) == task.getId()) return task;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public @NotNull Task removeOneById(final @NotNull String userId, final @NotNull String id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Nullable
    @Override
    public @NotNull Task findOneByIndex(final @NotNull String userId, final @NotNull Integer index) {
        final int arraySize = tasks.size();
        int iteration = 0;
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) {
                if (index <= arraySize && iteration == index) return tasks.get(index);
            }
            iteration++;
        }
        return null;
    }

    @Nullable
    @Override
    public @NotNull List<Task> findOneByName(final @NotNull String userId, final @NotNull String name) {
        final List<Task> projectsSample = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) {
                if (name.equals(task.getName())) projectsSample.add(task);
            }
        }
        return projectsSample;
    }

    @Nullable
    @Override
    public @NotNull Task removeOneByIndex(final @NotNull String userId, final @NotNull Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public @NotNull List<Task> removeOneByName(final @NotNull String userId, final @NotNull String name) {
        final List<Task> tasks = findOneByName(userId, name);
        if (tasks == null) return null;
        for (Task task : tasks) {
            remove(userId, task);
        }
        return tasks;
    }

    @Nullable
    @Override
    public @NotNull List<Task> getTasksList() {
        return tasks;
    }

    @Nullable
    @Override
    public void add(final @NotNull List<Task> task) {
        for (final Task tsk : task) {
            if (tsk == null) return;
            tasks.add(tsk);
        }
    }

    @NotNull
    @Override
    public void load(final @NotNull List<Task> tasks) {
        add(tasks);
    }

}
