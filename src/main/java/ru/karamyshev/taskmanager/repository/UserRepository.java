package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.IUserRepository;
import ru.karamyshev.taskmanager.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Nullable
    @Override
    public @NotNull List<User> findAll() {
        return users;
    }

    @Nullable
    @Override
    public @NotNull User add(final @NotNull User user) {
        users.add(user);
        return user;
    }

    @Nullable
    @Override
    public User findById(final @NotNull String id) {
        for (final User user : users) {
            String userId = Long.toString(user.getId());
            if (id.equals(userId)) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(final @NotNull String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public @NotNull User removeUser(@NotNull User user) {
        users.remove(user);
        return user;
    }

    @Nullable
    @Override
    public @NotNull User removeById(final @NotNull String id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Nullable
    @Override
    public User removeByLogin(final @NotNull String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

    @Nullable
    @Override
    public List<User> getUsersList() {
        return users;
    }

    @Nullable
    @Override
    public void add(final @NotNull List<User> userList) {
        for (final User user : userList) {
            if (user == null) return;
            users.add(user);
        }
    }

    @NotNull
    @Override
    public void load(final @NotNull List<User> userList) {
        add(userList);
    }

}
