package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.IProjectRepository;
import ru.karamyshev.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @NotNull
    @Override
    public void add(final @NotNull String userId, @NotNull final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @NotNull
    @Override
    public void remove(final @NotNull String userId, @NotNull final Project project) {
        projects.remove(project);
    }

    @Nullable
    @Override
    public @NotNull List<Project> findAll(@NotNull final String userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @NotNull
    @Override
    public void clear(@NotNull final String userId) {
        final List<Project> projects = findAll(userId);
        this.projects.removeAll(projects);
    }

    @Nullable
    @Override
    public Project findOneById(final @NotNull String userId, final @NotNull String id) {
        for (final Project project : projects) {
            if (userId.equals(project.getUserId()) &&
                    Long.parseLong(id) == project.getId()) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project removeOneById(final @NotNull String userId, @NotNull final String id) {
        final Project task = findOneById(userId, id);
        if (task == null) return null;
        projects.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final int arraySize = projects.size();
        int iteration = 0;
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) {
                if (index <= arraySize && iteration == index) return projects.get(index);
            }
            iteration++;
        }
        return null;
    }

    @Nullable
    @Override
    public List<Project> findOneByName(@NotNull final String userId, final @NotNull String name) {
        final List<Project> projectsSample = new ArrayList<>();
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) {
                if (name.equals(project.getName())) projectsSample.add(project);
            }
        }
        return projectsSample;
    }

    @Nullable
    @Override
    public @NotNull Project removeOneByIndex(@NotNull String userId, @NotNull final Integer index) {
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public List<Project> removeOneByName(@NotNull String userId, final @NotNull String name) {
        final List<Project> project = findOneByName(userId, name);
        if (project == null) return null;
        for (Project proj : project) {
            remove(userId, proj);
        }

        return project;
    }

    @Nullable
    @Override
    public List<Project> getProjectList() {
        return projects;
    }

    @Nullable
    @Override
    public void add(@NotNull final List<Project> project) {
        for (final Project prt : project) {
            if (prt == null) return;
            projects.add(prt);
        }
    }

    @NotNull
    @Override
    public void load(@NotNull final List<Project> projects) {
        add(projects);
    }

}
